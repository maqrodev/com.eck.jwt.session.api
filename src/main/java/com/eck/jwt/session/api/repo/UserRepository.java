package com.eck.jwt.session.api.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.eck.jwt.session.api.model.UserInfoModel;

public interface UserRepository extends JpaRepository<UserInfoModel, Integer> { 
    Optional<UserInfoModel> findByName(String username); 
}
